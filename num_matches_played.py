import csv

import matplotlib.pyplot as plt

def calculate_matches_by_team_season(matches_reader):
    """calculate_matches_by_team_season"""
    matches_by_team_season={}
    for match in matches_reader:
        season=match["season"]
        team1=match["team1"]
        team2=match["team2"]
        
    # Increment the match count for team1 in the current season
        matches_by_team_season[(season, team1)]=matches_by_team_season.get((season, team1),0)+1
       
    # Increment the match count for team2 in the current season
        matches_by_team_season[(season, team2)]=matches_by_team_season.get((season, team2),0)+1

    # print(matches_by_team_season)
        
    return matches_by_team_season


def plot_stacked_bar_chart(matches_by_team_season):
    """plot_stacked_bar_chart"""
    seasons = sorted(set(season for season, _ in matches_by_team_season.keys()))
    print(seasons)
    teams = sorted(set(team for _, team in matches_by_team_season.keys()))
    print(teams)
    res={team:[matches_by_team_season.get((season,team),0) for season in seasons]for team in teams}
    print(res)
    match_counts = {team: [matches_by_team_season.get((season, team), 0) for season in seasons] for team in teams}

    
    plt.figure(figsize=(10, 6))
    plt.bar(seasons, match_counts[teams[0]], label=teams[0])
    bottom = match_counts[teams[0]]
    
    for i in range(1, len(teams)):
        plt.bar(seasons, match_counts[teams[i]], bottom=bottom, label=teams[i])
        bottom = [bottom[j] + match_counts[teams[i]][j] for j in range(len(seasons))]
    
    plt.xlabel('Season')
    plt.ylabel('Number of Matches Played')
    plt.title('Matches Played by Team by Season')
    plt.legend()
    plt.xticks(rotation=45)
    plt.show()








def execute():
    """execute function"""
    with open('./archive/matches.csv', 'r',encoding='utf-8') as file:
        matches_reader = csv.DictReader(file)
        matches_by_team_season = calculate_matches_by_team_season(matches_reader)
        plot_stacked_bar_chart(matches_by_team_season)

execute()
