import csv

import matplotlib.pyplot as plt

def calculate_num_of_matches(match_details):
    """ calculate_num_of_matches"""
    num_matches_played={}

    for match in match_details:
        season=match["season"]
        num_matches_played[season]=num_matches_played.get(match["season"],0)+1
    print(num_matches_played)
    return dict(sorted(num_matches_played.items(),key=lambda item:item[0]))

def plot_num_of_matches_per_year(matches_played):
    """plot_num_of_matches_per_year"""
    season=matches_played.keys()
    matches=matches_played.values()

    plt.bar(season,matches)
    plt.xlabel=season
    plt.ylabel=matches
    plt.title("matches played per season")
    plt.xticks(rotation=45)
    plt.show()

def execut():
    with open("./archive/matches.csv","r",encode='utf-8') as file:
        match_details=csv.DictReader(file)
        result=calculate_num_of_matches(match_details)
        plot_num_of_matches_per_year(result)

execut()