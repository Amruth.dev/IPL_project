import csv

import matplotlib.pyplot as plt


def filter_matches_by_year(match_details,year):
    filter_match_by_year=[]
    for match in match_details:
        if(int(match['season'])==year):
            filter_match_by_year.append(match)
    return filter_match_by_year

def calculate__top_10_economical_bowlers(deliveries_details,year):
    """top_10_economical_bowlers"""
    bowler_runs = {}
    bowler_balls = {}
    for delivery in deliveries_details:
        if delivery['match_id'] in year:
            bowler = delivery['bowler']
            total_runs = int(delivery['total_runs'])
            bowler_runs[bowler] = bowler_runs.get(bowler, 0) + total_runs-(int(delivery["legbye_runs"])+int(delivery["bye_runs"]))
            bowler_balls[bowler] = bowler_balls.get(bowler, 0) + 1
    # print(bowler_balls)
    # print(bowler_runs)

    bowler_economy = {bowler: (bowler_runs[bowler] / bowler_balls[bowler]) * 6 for bowler in bowler_runs.keys()}
    sorted_bowler_economy = dict(sorted(bowler_economy.items(), key=lambda item: item[1]))
    print(dict(list(sorted_bowler_economy.items())[:10]))
    return dict(list(sorted_bowler_economy.items())[:10])



def plot_calculate__top_10_economical_bowlers(economy_of_each_bowler):
    """plot_calculate__top_10_economical_bowlers("""
    bowlers = list(economy_of_each_bowler.keys())
    economy_rates = list(economy_of_each_bowler.values())
    
    plt.figure(figsize=(10, 6))
    plt.barh(bowlers, economy_rates, color='skyblue')
    plt.xlabel('Economy Rate')
    plt.ylabel('Bowler')
    plt.title('Top 10 Most Economical Bowlers in 2016')
    plt.gca().invert_yaxis()
    plt.show()

def execute():
    year=2016
    with open("./archive/matches.csv","r",encoding="utf-8") as matches_file:
        match_detail=csv.DictReader(matches_file)
        matches_year=[match['id'] for match in filter_matches_by_year(match_detail,year)]


    with open("./archive/deliveries.csv","r",encoding="utf-8") as delivery_file:
        delivary_detail=csv.DictReader(delivery_file)
        economy_of_each_bowler=calculate__top_10_economical_bowlers(delivary_detail,matches_year)
        plot_calculate__top_10_economical_bowlers(economy_of_each_bowler)
execute()