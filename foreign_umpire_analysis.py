import csv
import matplotlib.pyplot as plt


def calculate_num_of_umpires(umpire_details):
   umpires_country={}
   for umpire in umpire_details:
            if(umpire[' country']!=' India'):   
                umpires_country[umpire['umpire']] = umpire[' country']
#    print(umpires_country)
   return umpires_country

   

def get_umpires_by_country(match_detail, umpires_country):
    # To keep track of foreign umpires already counted
    foreign_umpires_seen = set() 
    umpires_by_country = {}
    for match in match_detail:
         umpire1=match['umpire1']
         umpire2=match['umpire2']
         if umpire1 in umpires_country and umpire1 not in foreign_umpires_seen:
              umpires_by_country[umpires_country[umpire1]]=umpires_by_country.get(umpires_country[umpire1],0)+1
              foreign_umpires_seen.add(umpire1)
         if umpire2 in umpires_country and umpire2 not in foreign_umpires_seen:
              umpires_by_country[umpires_country[umpire2]]=umpires_by_country.get(umpires_country[umpire2],0)+1
              foreign_umpires_seen.add(umpire2)
        
    # print(umpires_by_country)
    
    return umpires_by_country

def plot_umpires_by_country(umpires_by_country):
    """plot_umpires_by_country"""
    plt.figure(figsize=(10, 6))
    plt.bar(umpires_by_country.keys(), umpires_by_country.values(), color='skyblue')
    plt.xlabel('Country')
    plt.ylabel('Number of Umpires')
    plt.title('Number of IPL Umpires by Country')
    plt.xticks(rotation=45)
    plt.show()


def execute():
    with open("./archive/umpires.csv","r",encoding="utf-8") as file:
        umpire_details=csv.DictReader(file)
        result=calculate_num_of_umpires(umpire_details)


    with open("./archive/matches.csv","r",encoding="utf-8") as file1:
        match_details=csv.DictReader(file1)
        result=get_umpires_by_country(match_details,result)
        plot_umpires_by_country(result)
execute()