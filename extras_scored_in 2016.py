import csv
import matplotlib.pyplot as plt

def filter_matches_by_year(matches_reader, year):
    """filter_matches_by_year"""
    filtered_matches = []
    for match in matches_reader:
        if match['season'] == year:
            filtered_matches.append(match)
    return filtered_matches


def calculate_extra_runs_per_team(deliveries_reader, matches_year):
    """calculate_extra_runs_per_team"""
    extra_runs_per_team = {}
    for delivery in deliveries_reader:
        if delivery['match_id'] in matches_year:
            bowling_team = delivery['bowling_team']
            extra_runs = int(delivery['extra_runs'])
            extra_runs_per_team[bowling_team] = extra_runs_per_team.get(bowling_team, 0) + extra_runs
    print(extra_runs_per_team)
    return extra_runs_per_team


def plot_extra_runs_per_team(extra_runs_per_team):
    """plot_extra_runs_per_team"""
    teams = list(extra_runs_per_team.keys())
    extra_runs = list(extra_runs_per_team.values())
    
    plt.bar(teams, extra_runs, color='skyblue')
    plt.xlabel('Teams')
    plt.ylabel('Extra Runs Conceded')
    plt.title('Extra Runs Conceded per Team in 2016')
    plt.xticks(rotation=45)
    plt.show()

def execute():
    year = '2016'
    with open('./archive/matches.csv', 'r',encoding="utf-8") as matches_file:
        matches_reader = csv.DictReader(matches_file)
        matches_year = [match['id'] for match in filter_matches_by_year(matches_reader, year)]
        # print(matches_year)
    
    # Read deliveries data
    with open('./archive/deliveries.csv', 'r',encoding="utf-8") as deliveries_file:
        deliveries_reader = csv.DictReader(deliveries_file)
        extra_runs_per_team = calculate_extra_runs_per_team(deliveries_reader, matches_year)
        plot_extra_runs_per_team(extra_runs_per_team)

execute()