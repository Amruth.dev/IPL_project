#import statements
import csv
import matplotlib.pyplot as plt


#calculate total runs function
def calculate_total_runs_by_team(matches_reader):
    """calculate_total_runs_by_team"""
    #initialized an empty object
    total_runs_by_team = {}
 
    for match in matches_reader:
        runs_scored = int(match['total_runs'])
        team = match['batting_team']
        total_runs_by_team[team] = total_runs_by_team.get(team, 0) + runs_scored
    return total_runs_by_team

def plot_total_runs_by_team(total_runs_by_team):
    " plot_total_runs_by_team"
    teams = list(total_runs_by_team.keys())
    runs = list(total_runs_by_team.values())
    plt.bar(teams, runs)
    plt.xlabel('Teams')
    plt.ylabel('Total Runs Scored')
    plt.title('Total Runs Scored by Each Team')
    plt.xticks(rotation=45)
    plt.show()


def execute():
    "execute module"
    with open('./archive/deliveries.csv', 'r',encoding='utf-8') as file:
        matches_reader = csv.DictReader(file)
        total_runs_by_team = calculate_total_runs_by_team(matches_reader)
        plot_total_runs_by_team(total_runs_by_team)
        
execute()


