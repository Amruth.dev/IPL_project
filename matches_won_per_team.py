import csv

import matplotlib.pyplot as plt


def matches_won_per_team(match_details):
    """matches_won_per_team"""
    match_won_per_team={}
  
    for match in match_details:
        year=match["season"]
        winner=match["winner"]

        if year not in match_won_per_team:
            match_won_per_team[year]={}
        match_won_per_team[year][winner]=match_won_per_team[year].get((winner),0)+1 
    # print(match_won_per_team)
    return match_won_per_team

def plot_matches_won_per_team(matches_won):
    # print(matches_won)
    teams = set()
    years = sorted(matches_won.keys())
    for year in years:
        teams.update(matches_won[year].keys())
    
    bottom = [0] * len(years)
    print(bottom)
    print(years)

    
    plt.figure(figsize=(10, 6))
     # print({matches_won['2008'].get('Rising Pune Supergiants',0)})
    for i, team in enumerate(teams):
        matches =[matches_won[year].get(team, 0) for year in years]
        
        plt.bar(years, matches, bottom=bottom, label=team)
        bottom = [bottom[j] + matches[j] for j in range(len(years))]

    plt.xlabel('Year')
    plt.ylabel('Number of Matches Won')
    plt.title('Number of Matches Won per Team per Year in IPL')
    plt.legend()
    plt.xticks(rotation=45)
    plt.show()
    

    return teams




def execute():
    """execute function"""
    with open("./archive/matches.csv","r",encoding='utf-8') as file:
        match_details=csv.DictReader(file)
        result=matches_won_per_team(match_details)
        plot_matches_won_per_team(result)


execute()