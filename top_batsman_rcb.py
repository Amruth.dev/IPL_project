import csv

import matplotlib.pyplot as plt


def top_10_run_getters_rcb(tournament_details):
    """total_runs_scored_by_rcb_batsman"""
    top_10_run_getters={}
    for player in tournament_details:
      if player["batting_team"] == "Royal Challengers Bangalore":
        runs=int(player["batsman_runs"])
        batsman=player["batsman"]
        top_10_run_getters[batsman]=top_10_run_getters.get(batsman,0) + runs
    top_10=dict(sorted(top_10_run_getters.items(),key=lambda item:item[1],reverse=True)[:10])
    
    return top_10


def plot_top_10_run_getters_rcb(top_run_getters):
    " plot_total_runs_by_team"

    player=top_run_getters.keys()
    runs=top_run_getters.values()
    plt.bar(player,runs)
    plt.xlabel(player)
    plt.ylabel(runs)
    plt.title("top 10 run getters for RCB")
    plt.xticks(rotation=45)
    plt.show()

def execute():
    "execute module"
    with open("./archive/deliveries.csv",'r',encoding='utf-8') as file:
        match_details=csv.DictReader(file)
        top_10_run_getters=top_10_run_getters_rcb(match_details)
        plot_top_10_run_getters_rcb(top_10_run_getters)


execute()
